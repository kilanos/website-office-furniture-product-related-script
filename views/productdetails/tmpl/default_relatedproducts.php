<?php
/**
 *
 * Show the product details page
 *
 * @package	VirtueMart
 * @subpackage
 * @author Max Milbers, Valerie Isaksen

 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default_relatedproducts.php 6431 2012-09-12 12:31:31Z alatak $
 */
 
// Check to ensure this file is included in Joomla!
defined ( '_JEXEC' ) or die ( 'Restricted access' );

//define custom query db
$db =& JFactory::getDBO();

//declaration of classes to hook current price
$model = new VirtueMartModelProduct();
$calculator = calculationHelper::getInstance();
$currency = CurrencyDisplay::getInstance();

?>
        <div class="product-related-products">
    	<h4><?php echo JText::_('COM_VIRTUEMART_RELATED_PRODUCTS'); ?></h4>

    <?php			
	
    foreach ($this->product->customfieldsRelatedProducts as $field) 
	{
	    if(!empty($field->display)) 
		{		
				
			$product = $model->getProductSingle($field->custom_value,false);
			$price = $calculator -> getProductPrices($product);		
			
			/* to view available price */
			//echo '<pre>';
			//print_r($price);
			//echo '</pre>';
			
			//custom query to display following requirements
			$queryCus = 'SELECT #__virtuemart_products_en_gb.product_s_desc, #__virtuemart_products_en_gb.product_name,
					     #__virtuemart_product_prices.product_price, #__virtuemart_product_prices.product_override_price
					     FROM `#__virtuemart_products_en_gb`
			             LEFT JOIN #__virtuemart_product_prices ON #__virtuemart_product_prices.virtuemart_product_id = #__virtuemart_products_en_gb.virtuemart_product_id
			             WHERE #__virtuemart_products_en_gb.virtuemart_product_id = '.$field->custom_value.'';
					  
			$db->setQuery($queryCus);  
			$customData = $db->loadObjectList();
			
			//product link
			$prodLink = JRoute::_('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $field->custom_value);
			
			//get the difference of price override
			$overrideDiff = (round($customData[0]->product_override_price, 2) - $currency->priceDisplay($price['salesPrice']));
	?>
			<div class="product-field product-field-type-<?php echo $field->field_type ?>">
				<span class="product-field-display"><?php echo $field->display ?></span><br/>
				<span class="product-field-title"><a href="<?php echo $prodLink; ?>"><?php echo $customData[0]->product_name; ?></a></span><br/>
				<span class="product-field-desc"><?php echo $customData[0]->product_s_desc; ?></span><br/>															  				
				<span class="product-field-final-price"> Price: <?php echo $currency->priceDisplay($price['salesPrice']); ?> </span><br/>				
				<?php
					if($customData[0]->product_override_price != '0.00') 
					{
				?>
						<span class="product-field-final-price"> Product override (sale) Price: <?php echo  $currency->priceDisplay($customData[0]->product_override_price); ?> </span><br/>
						<span class="product-field-final-price"> Difference of the override and the Final Price: <?php echo  $currency->priceDisplay($overrideDiff); ?> </span>
				<?php
					}
				?>
				
			</div>
	<?php 
		}
	 } 
	 ?>
    </div>
